module gitlab.com/luckshot/mfa-token

go 1.14

require (
	github.com/aws/aws-sdk-go-v2 v0.23.0
	github.com/jmespath/go-jmespath v0.3.0 // indirect
)
