# Vars
name      := mfa-token
builddir  := build
platforms := linux darwin windows
releasedir = $(builddir)/$@

goopts    := -trimpath -ldflags "-w -s"
gopkgs    := $(shell go list ./... | grep -v vendor)
gofiles   := $(foreach dir,$(shell go list -f '{{.Dir}}' ./... | grep -v vendor),$(wildcard $(dir)/*.go))

.PHONY: all
all: test app zip
.PHONY: release
release: $(platforms)


.PHONY: test
test:
	@echo "checking formatting..."
	@[ "$$(go fmt $(gopkgs) | wc -l)" = "0" ] || exit 1
	go vet $(gopkgs)
	golint -set_exit_status $(gopkgs)
	CGO_ENABLED=1 go test -race $(gopkgs)

.PHONY: coverage
coverage:
	CGO_ENABLED=0 go test -cover $(gopkgs) |\
		awk '{sum += $$5} END {printf "Coverage: %s%%\n", sum/NR}'

.PHONY: $(platforms)
$(platforms): $(gofiles)
	mkdir -p $(releasedir)
	CGO_ENABLED=0 GOOS=$@  go build $(goopts) -o $(releasedir) ./cmd/$(name)/...
	gzip -k $(releasedir)/*
	mv $(releasedir)/*.gz $(builddir)/$(name)_${@}.gz

.PHONY: clean
clean:
	rm -rf $(builddir)
