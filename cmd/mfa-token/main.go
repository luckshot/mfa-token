package main

import (
	"log"
	"os"
)

func main() {
	log.SetPrefix("[Error] ")
	log.SetFlags(0)
	if err := run(os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}
