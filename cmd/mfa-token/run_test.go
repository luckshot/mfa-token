package main

import (
	"bytes"
	"testing"
)

func TestExpiry(t *testing.T) {
	err := run([]string{"-exp"})
	if err != nil {
		t.Fatal(err)
	}
}

func TestStatus(t *testing.T) {
	var o bytes.Buffer
	msg := "Test message!"
	status(true, &o, msg)
	if o.String() != msg+"\n" {
		t.Fatal(o.String())
	}
}
