package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"gitlab.com/luckshot/mfa-token/token"
)

const (
	profileEnv    = "AWS_PROFILE"
	defaultRegion = "AWS_REGION"
	defaultRole   = "AWS_ROLE_ARN"
	mfaDevice     = "AWS_MFA_DEVICE"
	roleFileEnv   = "AWS_ROLE_FILE"
	sessDur       = "AWS_SESSION_DURATION"
	sessExpEnv    = "AWS_SESSION_EXP"
)

func status(v bool, w io.Writer, msg string) {
	if v {
		fmt.Fprintln(w, msg)
	}
}

func run(args []string) error {
	c := token.ParseConfig(
		profileEnv,
		defaultRegion,
		defaultRole,
		roleFileEnv,
		mfaDevice,
		sessDur,
		args,
	)

	if c.Expiry {
		status(true, os.Stdout, token.Expiry(sessExpEnv))
		return nil
	}

	status(c.Verbose, os.Stderr, "fetching keys...")
	conf, err := token.LoadLocalConfig(c.Profile, c.Region)
	if err != nil {
		return err
	}

	status(c.Verbose, os.Stderr, "fetching MFA serial number...")
	mfa := c.Profile
	if c.MFADevice != "" {
		mfa = c.MFADevice
	}
	serial, err := token.GetMFADevice(conf, mfa)
	if err != nil {
		return err
	}

	if !c.RoleIsARN && c.Role != "" {
		status(c.Verbose, os.Stderr, "looking up ARN...")
		fh, err := os.Open(c.RoleFile)
		if err != nil {
			return err
		}

		role, err := token.LookupRole(c.Role, fh)
		fh.Close()
		if err != nil {
			return err
		}
		c.Role = role
	}

	code, err := token.Code(os.Stdin, os.Stdout)
	if err != nil {
		return err
	}

	status(c.Verbose, os.Stderr, "fetching credentials...")
	creds, err := token.GetToken(conf, serial, code, c.Role, c.Duration)
	if err != nil {
		return err
	}

	byt := bytes.Buffer{}
	switch c.Format {
	case "json":
		byt.WriteString(creds.JSON())
	case "shell":
		byt.WriteString(creds.Shell())
	default:
		byt.WriteString(creds.String())
	}

	if c.Stdout {
		status(true, os.Stdout, byt.String())
		return nil
	}

	path := token.CredName(c.Profile, c.Role)
	if err := ioutil.WriteFile(path, byt.Bytes(), 0644); err != nil {
		// dump to stdout on err
		status(true, os.Stdout, byt.String())
		return err
	}
	status(true, os.Stderr, "Credentials saved in: "+path)
	return nil
}
