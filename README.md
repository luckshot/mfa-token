# MFA Token

Managing AWS session credentials becomes annoying when dealing with
multiple accounts and roles. This is compounded by the lack of an easy
way to convert the JSON output of the AWS CLI into something that can be
sourced into environment variables by the shell.

**MFA Token** is one attempt to solve this problem.

Once credentials are obtained, they are stored by name, so that
switching between roles is simply a matter of sourcing the file named
for that role.

## Installation

MFA Token is delivered as a single Gzipped binary for Linux, Windows, and
MacOS platforms. (For information on using it on Windows, see [issue
\#3](https://gitlab.com/luckshot/mfa-token/-/issues/3).)

Releases can be found on the [Release
page](https://gitlab.com/luckshot/mfa-token/-/releases).

The zipped files have a platform suffix (`mfa-token_linux.gz`) attached.
Using `gunzip -N` will restore the non-suffixed file name on Linux and
Mac. On Windows `7-Zip` will correctly extract `mfa-token_windows.gz` to
`mfa-token.exe`.

## Usage

MFA Token generates MFA secured AWS credentials for either a session
token or an assumed role. It can use an optional role file to provide
shortcut names for the AWS ARNs.

The default output is bash shell environment variables that can be
sourced directly. The file is saved to the temporary directory and its
location printed to console.

If no role is given, a STS session token will be generated.

If the `-print` flag is used, the credentials will be printed to the
terminal. Then the output can be captured with `eval` for direct
sourcing (`eval $(mfa-token -print)`).

Setting environment variables will allow the app to be run with no
options for convenience.

Requirements:

  - AWS account keys in the file `~/.aws/credentials` or
    `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` set in the
    environment.
  - A profile name if credentials other than `default` are to be used.

Note: the AWS CLI is not required to be installed.

## Examples

To assume the role `OrganizationalAccountAccessRole` in the account
123456789021, with the keys stored in the `mouse` AWS credential
profile:

``` bash
mfa-token -p mouse -r arn:aws:iam::123456789021:role/OrganizationalAccountAccessRole
Enter MFA code: xxxxxx
Credentials saved in: /tmp/mouse-123456789021-OrganizationalAccountAccessRole.aws
```

If that role is stored as `oaar` in the roles file:

``` bash
mfa-token -p mouse -r oaar
Enter MFA code: xxxxxx
Credentials saved in: /tmp/mouse-123456789021-OrganizationalAccountAccessRole.aws
```

If `AWS_PROFILE` is set to `mouse`:

``` bash
mfa-token -r oaar
Enter MFA code: xxxxxx
Credentials saved in: /tmp/mouse-123456789021-OrganizationalAccountAccessRole.aws
```

## Options

The following environment variables will be used if present:

  - `AWS_PROFILE`: AWS profile used to choose keys
  - `AWS_MFA_DEVICE`: Name of MFA device
  - `AWS_REGION`: STS regional endpoint to use
  - `AWS_ROLE_ARN`: AWS role to assume
  - `AWS_ROLE_FILE`: Location of the role file
  - `AWS_SESSION_DURATION`: Amount of time the token will be valid for

MFA Token has the following command options:

``` txt
$ mfa-token -h
Usage:
  -d duration
        Token duration or AWS_SESSION_DURATION (default 1h0m0s)
  -exp
        Output session expiration time
  -f string
        Location of JSON-formatted role file or AWS_ROLE_FILE (default "/home/<user>/.config/sts-roles.json")
  -format string
        Output in [shell|json] format (default "shell")
  -m string
        Name of MFA device to use or AWS_MFA_DEVICE
  -p string
        AWS profile or AWS_PROFILE (default "default")
  -print
        Print credentials to terminal
  -r string
        Role to assume or AWS_ROLE_ARN
  -region string
        Default AWS region or AWS_REGION (default "us-east-1")
  -verbose
        Print progress messages
```

### MFA device name

If MFA is enabled via the AWS Console, the device name is usually set to
be the same as the IAM user name. When it’s enabled via the CLI, a user
has the option of what to name the device. This option reflects that
choice.

In most cases, it won’t need to be set. If your MFA device name is
different from your IAM user name, you know what you did, and should set
this option.

The MFA device name is checked against AWS and will error before
`mfa-token` asks for the MFA code.

## Role File

The role file contains a list of named roles that `mfa-token` can use
when generating STS credentials. Each item consists of a shortcut name
and the ARN of the role.

By default `mfa-token` looks for this file in in the user’s config
directory, with the name `sts-roles.json`. The help message will print
the full path to the config directory.

``` json
{
  "role_one": "arn:aws:iam::<account>:role/<role_one_name>",
  "role_two": "arn:aws:iam::<account>:role/<role_two_name>"
}
```

If a role is provided on the command line or in the AWS\_ROLE\_ARN
environment variable that is a valid ARN, the file lookup will be
skipped.

### AWS credential file

The AWS CLI is installed the credential file can be set up correctly
with the `aws configure` command. Otherwise the file can be constructed
in a text editor with the following INI format:

``` ini
[name]
aws_access_key_id = <key>
aws_secret_access_key = <secret>
```

The default location is `~/.aws/credentials`.
