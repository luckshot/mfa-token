package token

import "testing"

func TestMFAName(t *testing.T) {
	devs := []struct {
		serial string
		name   string
	}{
		{"arn:aws:iam::123456789012:mfa/username", "username"},
		{"arn:aws:iam::123456789012:mfa/nameuser", "nameuser"},
		{"arn:aws:iam::123456789012:mfa/root-account-mfa-device", "root-account-mfa-device"},
	}

	for k, v := range devs {
		s, d := mfaName(v.serial)
		if d != v.name {
			t.Errorf("%d: %s : %s", k, v.name, d)
		}
		if s != v.serial {
			t.Errorf("%d: %s : %s", k, v.serial, s)
		}
	}
}
