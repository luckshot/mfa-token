package token

import (
	"context"
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/iam"
)

// GetMFADevice ...
func GetMFADevice(cfg aws.Config, device string) (string, error) {
	cfg.Region = "aws-global"

	svc := iam.New(cfg)
	input := &iam.ListVirtualMFADevicesInput{
		AssignmentStatus: "Assigned",
	}
	req := svc.ListVirtualMFADevicesRequest(input)

	resp, err := req.Send(context.Background())
	if err != nil {
		return "", err
	}

	for _, v := range resp.VirtualMFADevices {
		serial, name := mfaName(*v.SerialNumber)
		if name == device {
			return serial, nil
		}
	}
	return "", fmt.Errorf("No MFA devices found matching: %s", device)
}

func mfaName(device string) (string, string) {
	sp := strings.SplitN(device, "/", 2)
	return device, sp[1]
}
