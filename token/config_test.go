package token

import (
	"os"
	"path/filepath"
	"testing"
)

func TestParseFlag(t *testing.T) {
	t.Cleanup(func() { os.Unsetenv("ONE") })
	os.Setenv("ONE", "my_profile")

	fl := ParseConfig("ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", nil)
	if fl.Profile != "my_profile" {
		t.Fatal(fl.Profile)
	}
}

func TestBadDuration(t *testing.T) {
	t.Cleanup(func() { os.Unsetenv("FOUR") })
	os.Setenv("FOUR", "wrong")

	fl := ParseConfig("ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", nil)
	if fl.Duration != 3600 {
		t.Fatal(fl.Duration)
	}
}

func TestCredName(t *testing.T) {
	p := "pro"
	tmp := os.TempDir()

	t.Run("with ARN", func(t *testing.T) {
		r := "arn:aws:iam::123456789012:assume-role/role/DevRole"
		res := CredName(p, r)

		if res != filepath.Join(tmp, "pro-123456789012-DevRole.aws") {
			t.Fatal(res)
		}
	})

	t.Run("w/o ARN", func(t *testing.T) {
		r := "myrole"
		res := CredName(p, r)

		if res != filepath.Join(tmp, "pro-myrole.aws") {
			t.Fatal(res)
		}
	})

	t.Run("session", func(t *testing.T) {
		r := ""
		res := CredName(p, r)

		if res != filepath.Join(tmp, "pro.aws") {
			t.Fatal(res)
		}
	})
}

func TestLoadCreds(t *testing.T) {
	_, err := LoadLocalConfig("default", "region")
	if err != nil {
		t.Fatal(err)
	}
}
