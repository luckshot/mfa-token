package token

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"regexp"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sts"
)

// Creds ...
type Creds struct {
	sts.Credentials
}

// String ...
func (c Creds) String() string {
	const s = `{
	AccessKeyID: %q
	SecretAccessKey: %q
	Expiration: %q
	SessionToken: %q
}`

	return fmt.Sprintf(s, *c.AccessKeyId, *c.SecretAccessKey, c.Expiration.Format(time.RFC3339), *c.SessionToken)
}

// Shell ...
func (c Creds) Shell() string {
	const s = `export AWS_ACCESS_KEY_ID=%q;
export AWS_SECRET_ACCESS_KEY=%q;
export AWS_SESSION_TOKEN=%q;
export AWS_SESSION_EXP=%q;
echo "Credentials expire: %s"`

	return fmt.Sprintf(s, *c.AccessKeyId, *c.SecretAccessKey, *c.SessionToken,
		c.Expiration.Format(time.RFC3339),
		c.Expiration.Local().Format(time.RFC850),
	)
}

// JSON ...
func (c Creds) JSON() string {
	b, err := json.MarshalIndent(c, "", "  ")
	if err != nil {
		return ""
	}
	return string(b)
}

// GetToken ...
func GetToken(conf aws.Config, serial, token, role string, dur int64) (Creds, error) {
	if role == "" {
		return getSessionToken(conf, serial, token, dur)
	}
	if token == "" && serial == "" {
		return assumeRole(conf, role, dur)
	}
	return assumeRoleMFA(conf, serial, token, role, dur)
}

func assumeRole(conf aws.Config, role string, dur int64) (Creds, error) {
	svc := sts.New(conf)
	input := &sts.AssumeRoleInput{
		DurationSeconds: aws.Int64(dur),
		RoleArn:         aws.String(role),
		RoleSessionName: aws.String("sts-token"),
	}

	req := svc.AssumeRoleRequest(input)
	resp, err := req.Send(context.Background())
	if err != nil {
		return Creds{}, err
	}
	return Creds{*resp.Credentials}, nil
}

func assumeRoleMFA(conf aws.Config, serial, token, role string, dur int64) (Creds, error) {
	svc := sts.New(conf)
	input := &sts.AssumeRoleInput{
		DurationSeconds: aws.Int64(dur),
		SerialNumber:    aws.String(serial),
		TokenCode:       aws.String(token),
		RoleArn:         aws.String(role),
		RoleSessionName: aws.String("mfa-token"),
	}

	req := svc.AssumeRoleRequest(input)
	resp, err := req.Send(context.Background())
	if err != nil {
		return Creds{}, err
	}
	return Creds{*resp.Credentials}, nil
}

func getSessionToken(conf aws.Config, serial, token string, dur int64) (Creds, error) {
	svc := sts.New(conf)
	input := &sts.GetSessionTokenInput{
		DurationSeconds: aws.Int64(dur),
		SerialNumber:    aws.String(serial),
		TokenCode:       aws.String(token),
	}

	req := svc.GetSessionTokenRequest(input)
	resp, err := req.Send(context.Background())
	if err != nil {
		return Creds{}, err
	}
	return Creds{*resp.Credentials}, nil
}

// Code ...
func Code(r io.Reader, w io.Writer) (string, error) {
	var code string

	fmt.Fprint(w, "Enter MFA code: ")
	_, err := fmt.Fscanln(r, &code)
	if err != nil {
		return "", err
	}

	if len(code) != 6 {
		return "", errors.New("code is not 6 digits")
	}

	re := regexp.MustCompile(`\d{6}`)
	if !re.MatchString(code) {
		return "", errors.New("code contains non-numerals")
	}
	return code, nil
}

// Expiry ...
func Expiry(env string) string {
	e, ok := os.LookupEnv(env)
	if !ok {
		return "No active session found"
	}
	exp, err := time.Parse(time.RFC3339, e)
	if err != nil {
		return err.Error()
	}

	if time.Now().After(exp) {
		return "Session expired at: " + exp.Local().Format(time.RFC850)
	}
	return "Session expires: " + exp.Local().Format(time.RFC850)
}
