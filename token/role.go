package token

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"

	"github.com/aws/aws-sdk-go-v2/aws/arn"
)

// LookupRole validates a role.
func LookupRole(role string, roleData io.Reader) (string, error) {
	if role == "" {
		return "", nil
	}
	if validARN(role) {
		return role, nil
	}
	if roleData == nil {
		return "", errors.New("empty io.Reader")
	}

	roles := make(map[string]string)
	if err := json.NewDecoder(roleData).Decode(&roles); err != nil {
		return "", err
	}

	r, ok := roles[role]
	if !ok {
		return "", fmt.Errorf("role %q not found", role)
	}
	if !validARN(r) {
		return "", fmt.Errorf("invalid ARN: %q", r)
	}
	return r, nil
}

func validARN(r string) bool {
	if !arn.IsARN(r) {
		return false
	}
	_, err := arn.Parse(r)
	if err != nil {
		return false
	}
	return true
}
