package token

import (
	"bytes"
	"os"
	"strings"
	"testing"
	"time"
)

func TestCodeEntry(t *testing.T) {
	r := strings.NewReader("678765")
	w := bytes.Buffer{}
	c, err := Code(r, &w)
	if err != nil {
		t.Fatal(err)
	}
	if c != "678765" {
		t.Fatal(c)
	}
}

func TestCodeFailLong(t *testing.T) {
	r := strings.NewReader("6787765")
	w := bytes.Buffer{}
	_, err := Code(r, &w)
	if err.Error() != "code is not 6 digits" {
		t.Fatal(err)
	}
}

func TestCodeFailShort(t *testing.T) {
	r := strings.NewReader("665")
	w := bytes.Buffer{}
	_, err := Code(r, &w)
	if err.Error() != "code is not 6 digits" {
		t.Fatal(err)
	}
}

func TestCodeAlpha(t *testing.T) {
	r := strings.NewReader("66534A")
	w := bytes.Buffer{}
	_, err := Code(r, &w)
	if err.Error() != "code contains non-numerals" {
		t.Fatal(err)
	}
}

func TestExpiryOk(t *testing.T) {
	tm := time.Now().Add(time.Hour)
	os.Setenv("EXP", tm.Format(time.RFC3339))
	t.Cleanup(func() { os.Unsetenv("EXP") })

	e := Expiry("EXP")
	msg := "Session expires: " + tm.Format(time.RFC850)
	if e != msg {
		t.Fatal(e)
	}
}

func TestExpired(t *testing.T) {
	tm := time.Now().Add(-time.Hour)
	os.Setenv("EXP", tm.Format(time.RFC3339))
	t.Cleanup(func() { os.Unsetenv("EXP") })

	e := Expiry("EXP")
	msg := "Session expired at: " + tm.Format(time.RFC850)
	if e != msg {
		t.Fatal(e)
	}
}

func TestExpiryNil(t *testing.T) {
	e := Expiry("FOO")
	if e != "No active session found" {
		t.Fatal(e)
	}
}
