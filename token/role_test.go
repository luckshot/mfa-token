package token

import (
	"strings"
	"testing"
)

func TestValidARN(t *testing.T) {
	arn := "arn:aws:service:region:account:resource"
	ok := validARN(arn)
	if !ok {
		t.Fatal("invalid")
	}
}

func TestInvalidARN(t *testing.T) {
	ok := validARN("aws:service:region:account:resource")
	if ok {
		t.Fatal("falsly valid")
	}
}

func TestLookup(t *testing.T) {
	file := `{
	"foo": "arn:aws:iam::*:role/somerole",
	"bar": "ran:aws:iam::*:role/another"
}`

	t.Run("arn", func(t *testing.T) {
		_, err := LookupRole("arn:aws:service:region:account:resource/id", nil)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("nil", func(t *testing.T) {
		_, err := LookupRole("foo", nil)
		if err.Error() != "empty io.Reader" {
			t.Fatal(err)
		}
	})

	t.Run("valid", func(t *testing.T) {
		r, err := LookupRole("foo", strings.NewReader(file))
		if err != nil {
			t.Fatal(err)
		}

		if r != "arn:aws:iam::*:role/somerole" {
			t.Fatal(r)
		}
	})

	t.Run("invalid", func(t *testing.T) {
		_, err := LookupRole("bar", strings.NewReader(file))
		if err.Error() != `invalid ARN: "ran:aws:iam::*:role/another"` {
			t.Fatal(err)
		}
	})

	t.Run("missing", func(t *testing.T) {
		_, err := LookupRole("baz", strings.NewReader(file))
		if err.Error() != `role "baz" not found` {
			t.Fatal(err)
		}
	})
}
