package token

import (
	"flag"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/arn"
	"github.com/aws/aws-sdk-go-v2/aws/external"
)

// Flags holds the config data.
type Flags struct {
	Format    string
	Profile   string
	Region    string
	Role      string
	RoleFile  string
	MFADevice string
	Duration  int64
	RoleIsARN bool
	Expiry    bool
	Stdout    bool
	Verbose   bool
}

func fetchEnv(env, def string) string {
	e, ok := os.LookupEnv(env)
	if !ok {
		return def
	}
	return e
}

// ParseConfig returns the cmd line flags converted to a Flags struct.
func ParseConfig(profile, region, roleName, roleFile, mfaDevice, sessDef string, args []string) Flags {
	prof := fetchEnv(profile, "default")
	regi := fetchEnv(region, "us-east-1")
	sdur := fetchEnv(sessDef, "1h")
	role := fetchEnv(roleName, "")
	path, _ := os.UserConfigDir()
	rfil := fetchEnv(roleFile, filepath.Join(path, "sts-roles.json"))
	mfad := fetchEnv(mfaDevice, "")

	sessDur, err := time.ParseDuration(sdur)
	if err != nil {
		sessDur = time.Hour
	}

	set := flag.NewFlagSet("", flag.ExitOnError)
	var (
		fFormat    = set.String("format", "shell", "Output in [shell|json] format")
		fProfile   = set.String("p", prof, "AWS profile or "+profile)
		fRegion    = set.String("region", regi, "Default AWS region or "+region)
		fRole      = set.String("r", role, "Role to assume or "+roleName)
		fRoleFile  = set.String("f", rfil, "Location of JSON-formatted role file or "+roleFile)
		fMFADevice = set.String("m", mfad, "Name of MFA device to use or "+mfaDevice)
		fDuration  = set.Duration("d", sessDur, "Token duration or "+sessDef)
		fExp       = set.Bool("exp", false, "Output session expiration time")
		fStdout    = set.Bool("print", false, "Print credentials to terminal")
		fVerb      = set.Bool("verbose", false, "Print progress messages")
	)
	set.Parse(args)

	f := Flags{
		Format:    *fFormat,
		Profile:   *fProfile,
		Region:    *fRegion,
		Role:      *fRole,
		RoleIsARN: validARN(*fRole),
		RoleFile:  *fRoleFile,
		MFADevice: *fMFADevice,
		Duration:  int64(fDuration.Seconds()),
		Expiry:    *fExp,
		Stdout:    *fStdout,
		Verbose:   *fVerb,
	}
	return f
}

// LoadLocalConfig ...
func LoadLocalConfig(profile, region string) (aws.Config, error) {
	c, err := external.LoadDefaultAWSConfig(
		external.WithSharedConfigProfile(profile),
		external.WithRegion(region), // can't count on a region being set
	)
	if err != nil {
		return aws.Config{}, err
	}
	return c, nil
}

// CredName ...
func CredName(p, r string) string {
	var out []string
	tmp := os.TempDir()
	out = append(out, p)

	if arn.IsARN(r) {
		if a, err := arn.Parse(r); err == nil {
			out = append(out, a.AccountID)
			s := strings.Split(a.Resource, "/")
			out = append(out, s[len(s)-1])
			return filepath.Join(tmp, strings.Join(out, "-")+".aws")
		}
	}

	// Non-ARN role name from roles.json
	if r != "" {
		out = append(out, r)
	}
	return filepath.Join(tmp, strings.Join(out, "-")+".aws")
}
